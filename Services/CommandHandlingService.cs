using System;
using System.Reflection;
using System.Threading.Tasks;
using DSharpPlus;
using DSharpPlus.EventArgs;

namespace sharpbot.Services
{
    public class CommandHandlingService
    {
        private readonly DiscordClient _discord;
        private IServiceProvider _provider;

        public CommandHandlingService(IServiceProvider provider, DiscordClient discord)
        {
            _discord = discord;
            _provider = provider;

            //_discord.MessageCreated += MessageReceived;
        }


        public async Task InitializeAsync(IServiceProvider provider)
        {
            _provider = provider;
            await Task.CompletedTask;
        }        

        private async Task MessageReceived(MessageCreateEventArgs e)
        {
            if(e.Author == _discord.CurrentUser){
                return;
            }
            await e.Message.RespondAsync(e.Message.Author.Username);
        }
    }


}