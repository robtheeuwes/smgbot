﻿using System;
using System.IO;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using System.Collections.Generic;

using DSharpPlus;

using DSharpPlus.EventArgs;
using DSharpPlus.CommandsNext;
using DSharpPlus.Entities;
using Newtonsoft.Json;

using sharpbot.Services;
using sharpbot.Command;
using sharpbot.model;


namespace sharpbot
{
    public class Program
    {

        private Config config {get; set;}
        private DiscordClient _client;

        private CommandCollection commands;

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            Config config = LoadConfig();

            new Program(config).MainAsync().GetAwaiter().GetResult();
        }
        public Program(Config config)
        {
            this.config = config;
            // It is recommended to Dispose of a client when you are finished
            // using it, at the end of your app's lifetime.
            _client = new DiscordClient(new DiscordConfiguration{
                Token = config.Token,
                TokenType = TokenType.Bot,
                MinimumLogLevel = LogLevel.Debug
            });
        }

        public async Task MainAsync()
        {
            var services = ConfigureServices();
            await services.GetRequiredService<CommandHandlingService>().InitializeAsync(services);

            this.commands = new CommandCollection( _client );
            commands.Add( new ReviewCommandHandler( this.config ) );
            commands.Add( new TrentWatcher( this.config ) );
            //commands.Add( new JonHandler( this.config ) );
            //commands.Add( new FloydHandler( this.config) );
            
            _client.MessageCreated += OnMessageCreated;
            _client.Ready += ReadyAsync;

            await _client.ConnectAsync();
            await Task.Delay(-1);
        }

        private Task ReadyAsync(DiscordClient client, ReadyEventArgs e)
        {
            Console.WriteLine($"{_client.CurrentUser} is connected!");
            return Task.CompletedTask;
        }

        private async Task OnMessageCreated(DiscordClient client, MessageCreateEventArgs e)
        {
            // do not handle bot messages 

            if( e.Author.Id == this.config.BotUserId)
            {
                return;
            }
            
            /*
            handle by rule ... experimental 

            foreach( ConfigRule rule in this.config.rules)
            {
                if(rule.ChannelIds.Contains( e.Channel.Id ))
                {
                    Console.WriteLine( String.Format("found channel for rule : {0}", rule.Name));
                }
            }
            */
            this.commands.HandleMessage(e);
        }

        protected void SendMessageOnChannel(ulong ChannelId, string message)
        {
            DiscordChannel channel = _client.GetChannelAsync( this.config.Logchannel ).GetAwaiter().GetResult();
            channel.SendMessageAsync(message);
        }

        protected static Config LoadConfig()
        {
            string json = File.ReadAllText(@"./data/config.json");
            return JsonConvert.DeserializeObject<Config>( json );
        }

        private IServiceProvider ConfigureServices()
        {
            return new ServiceCollection()
                // Base
                .AddSingleton(_client)
                .AddSingleton<CommandHandlingService>()
                // Extra
                .AddSingleton(config)
                // Add additional services here...
                .BuildServiceProvider();
        }        

    }

}