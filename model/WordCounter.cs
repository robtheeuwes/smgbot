using System;
namespace model
{
    public class WordCounter
    {
        public DateTime startDate {get;set;}
        public int wordCount {get;set;} = 0;
        public WordCounter()
        {
            this.startDate = DateTime.Now;   
            this.wordCount = 0 ;
        }

        public int AddMessage(string message)
        {
            int cnt = (int) message.Split(' ').Length;
            this.wordCount += cnt;
            return this.wordCount;
        }

    }
}