using System.Collections.Generic;

namespace sharpbot.model
{
    public class ConfigRule
    {
        public string Name {get; set;}
        public List<ulong> ChannelIds {get;set;}

        public string Handler {get;set;}
    }
}