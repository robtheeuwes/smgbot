using System.Collections.Generic;
using sharpbot.Command;
using DSharpPlus;
using DSharpPlus.EventArgs;
using System.Linq;
namespace sharpbot.model
{
    public class CommandCollection : List<BaseCommand>
    {

        private DiscordClient client {get; set;}
        public CommandCollection(DiscordClient client)
        {
            this.client = client;
        }

        public BaseCommand GetHandlerByName(string name)
        {
            return this.First( i => i.GetName() == name);
        }

        public DiscordClient GetClient()
        {
            return this.client;
        }

        public void HandleMessage(MessageCreateEventArgs e)
        {
            for(int i = 0 ; i < this.Count ; i++){
                BaseCommand command = this.ElementAt<BaseCommand>(i);
                if(command is BaseCommand)
                {
                    if(command.handle(this, e)){
                        return;
                    }

                }
            }
        }

    }
}