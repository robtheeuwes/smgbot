using System.Collections.Generic;

namespace sharpbot.model
{
    
    public class Config
    {
        public string Name {get; set;}
        public string Token {get; set;}

        public ulong BotUserId {get; set;}

        public ulong JonmanUserId {get;set;}

        public ulong Logchannel {get; set;}

        public ulong ReviewChannelId {get; set;}

        public List<string> ReviewReminderMessages {get;set;}
        public List<string> JonmanResponses {get;set;}
        public List<string> JonmanForbiddenWords {get;set;}

        public List<ulong> StaffRoles {get;set;}
        public List<ulong> StaffUsers {get;set;}

        public List<ConfigRule> rules {get;set;}
    }
}