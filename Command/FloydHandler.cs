using System;
using DSharpPlus.EventArgs;
using sharpbot.model;

namespace sharpbot.Command
{

    public class FloydHandler : BaseCommand
    {

        private Config config { get; set; }
        public FloydHandler(Config config)
        {
            this.config = config;
        }

        public bool handle(CommandCollection commands, MessageCreateEventArgs e)
        {
            Console.WriteLine(string.Format("User {0} sent a message in channel {1}", e.Author.Username, e.Channel.Name));
            if( e.Message.ChannelId == 745806920094580790 )
            {
                if( this.MessageContainsFloyd(e.Message.Content) )
                {
                    e.Message.RespondAsync(string.Format("{0} : Fuck you and your Floyd Rose", e.Author.Mention));
                    return true;
                }            
            }
            return false;
        }

        protected string GetResponseMessage()
        {
            Random  rand = new Random();
            return this.config.ReviewReminderMessages[ rand.Next( this.config.ReviewReminderMessages.Count) ];
        }

        protected bool MessageContainsFloyd(string message)
        {
            if (message.ToLower().Contains("floyd"))
            {
                return true;
            }

            if (message.ToLower().Contains("floyd rose"))
            {
                return true;
            }

            return false;
        }

        public string GetName()
        {
            return "floyd";
        }
    }

}
