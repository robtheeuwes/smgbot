using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using DSharpPlus.EventArgs;
using DSharpPlus.Entities;
using sharpbot.model;
using System.Linq;

namespace sharpbot.Command
{

    public class NukeCommandHandler : BaseCommand
    {

        private Config config { get; set; }
        public NukeCommandHandler(Config config)
        {
            this.config = config;
        }

        public bool handle(CommandCollection commands, MessageCreateEventArgs e)
        {

            string[] parts = e.Message.Content.Split(" ");
            if(parts.Length >= 1)
            {
                if(parts[0].Equals("!nuke"))
                {

                    Console.WriteLine("NUKE ENGAGED");

                    if(!this.IsUserAllowed(e.Author, e.Guild)){
                        Console.WriteLine( string.Format("USer : {e} is not allowed to nuke", e.Author.Username));
                        return false;
                    }

                    
                    int numberOfMessages = 1000;
                    ulong channelId = e.Channel.Id;
                    if(parts.Length > 1){
                        numberOfMessages = Int16.Parse( parts[1] );
                    }

                    Console.WriteLine(string.Format("Nuking : {0} messages", numberOfMessages));

                    try{
                        List<DiscordMessage> messages = e.Channel.GetMessagesAsync(numberOfMessages).GetAwaiter().GetResult().ToList();
                        List<DiscordMessage> deleteMessages = new List<DiscordMessage>();

                        foreach(DiscordMessage msg in messages){
                            deleteMessages.Add(msg);
                            Console.WriteLine( string.Format("Deleting message : {0}", msg.Id) );
                        }


                        Console.WriteLine( string.Format("Num : {0}", deleteMessages.Count));

                        

                        e.Channel.DeleteMessagesAsync( deleteMessages ).GetAwaiter().GetResult();
                        //Console.WriteLine( string.Format("Message : {0}", msg.Id));
                    }
                    catch(Exception ex){

                        Console.WriteLine( string.Format("nuked failed: {0} : {1}", ex.Message,ex.StackTrace.ToString()));
                    }
                    

                    return true;
                }
            }
            return false;

    
        }

        public bool IsUserAllowed(DiscordUser user, DiscordGuild guild)
        {

            if(this.config.StaffUsers.Contains( user.Id)){
                return true;

            }
            return false;

            try{

                DiscordMember member = guild.GetMemberAsync(user.Id).GetAwaiter().GetResult();
                
                Console.WriteLine("member found " + member.Id);

                Console.WriteLine("member has " + member.Roles.Count() + " roles");
                

                foreach( var role in member.Roles)
                {
                    Console.WriteLine("Checking role: " + role.Id);
                    if(this.config.StaffRoles.Contains(role.Id))
                    {
                        return true;
                    }
                }

            }
            catch(Exception ex){
                Console.WriteLine("Ex :" + ex.Message);
            }
            return false;
        }

        public string GetName()
        {
            return "nuke";
        }
    }

}
