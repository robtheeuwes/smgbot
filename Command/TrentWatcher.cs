using System;
using DSharpPlus.EventArgs;
using sharpbot.model;
using DSharpPlus.Entities;
using System.Collections.Generic;
using model;

namespace sharpbot.Command
{

    public class TrentWatcher : BaseCommand
    {

        private Config config { get; set; }
        // trent
        protected ulong userId = 649858119001964565;

        // me 
        //protected ulong userId = 687966448672440409;
        protected int MaxWords {get;set;}
        protected int MaxMinutes {get;set;}
        protected WordCounter wordCounter;

        public TrentWatcher(Config config)
        {
            this.config = config;
            this.MaxWords = 500;
            this.MaxMinutes = 30;
            this.wordCounter = new WordCounter();
        }

        public bool handle(CommandCollection commands, MessageCreateEventArgs e)
        {
            if(e.Message.Author.Id != this.userId){
                return false;
            }

            // check if wordcounter expired 
            TimeSpan span = DateTime.Now - this.wordCounter.startDate;            
            if(span.Minutes > this.MaxMinutes){
                this.wordCounter = new WordCounter();
                return false;
            }

            int cnt = this.wordCounter.AddMessage(e.Message.Content);

            if(cnt > this.MaxWords)
            {
                e.Message.DeleteAsync().GetAwaiter().GetResult();
                e.Message.RespondAsync(string.Format("Sorry {0} You have reached your maximum of {1} words in {2} minutes", e.Author.Mention, this.MaxWords, this.MaxMinutes));

            }


            return true;
   
        }

        protected bool CheckMediaFileSize( DiscordAttachment attachment)
        {
            int fileSizeInKb = attachment.FileSize / 1024;
            if( attachment.FileName.ToLower().Contains(".mp3") && fileSizeInKb < 2000){
                return false;
            }
            
            if( attachment.FileName.ToLower().Contains(".wav") && fileSizeInKb < 10000){
                return false;
            }

            return true;
        }

        protected bool MessageContainsVideoAttachment(DiscordMessage message)
        {
            foreach( DiscordAttachment attachment in message.Attachments)
            {
                if(attachment.Url.ToLower().Contains(".mp4") || attachment.Url.ToLower().Contains(".webm") || attachment.Url.ToLower().Contains(".avi"))
                {
                    return true;
                }
            }

            return false;

        }

        protected bool MessageContainsAudioAttachment(DiscordMessage message)
        {
            foreach( DiscordAttachment attachment in message.Attachments)
            {
                if(attachment.Url.Contains(".mp3") || attachment.Url.Contains(".wav") || attachment.Url.Contains(".ogg"))
                {
                    return true;
                }
            }

            return false;
        }

        protected string GetResponseMessage()
        {
            Random  rand = new Random();
            return this.config.ReviewReminderMessages[ rand.Next( this.config.ReviewReminderMessages.Count) ];
        }
        protected bool MessageContainsLink(string message)
        {
            if (message.ToLower().Contains("http://"))
            {
                return true;
            }

            if (message.ToLower().Contains("https://"))
            {
                return true;
            }

            return false;
        }

        protected bool ContainsPublicLink(string message)
        {
            if (message.ToLower().Contains("youtube"))
            {
                return true;
            }
            
            if (message.ToLower().Contains("youtu.be"))
            {
                return true;
            }

            if (message.ToLower().Contains("soundcloud"))
            {
                return true;
            }
            return false;

        }

        public string GetName()
        {
            return "mmr";
        }
    }

}
