using System;
using DSharpPlus.EventArgs;
using sharpbot.model;

namespace sharpbot.Command
{

    public class JonHandler : BaseCommand
    {

        private Config config { get; set; }
        public JonHandler(Config config)
        {
            this.config = config;
            Console.WriteLine(config.JonmanForbiddenWords.ToString());
            Console.WriteLine(config.JonmanResponses.ToString());
        }

        public bool handle(CommandCollection commands, MessageCreateEventArgs e)
        {
            Console.WriteLine(string.Format("JONMAN : User {0} sent a message in channel {1}", e.Author.Username, e.Channel.Name));
            if( e.Message.Author.Id == config.JonmanUserId )
            {
                if( this.MessageContainsMetallica(e.Message.Content) )
                {
                    e.Message.RespondAsync(string.Format(this.GetResponseMessage(), e.Author.Mention));
                    return true;
                }            
            }
            return false;
        }

        protected string GetResponseMessage()
        {
            Random  rand = new Random();
            return this.config.JonmanResponses[ rand.Next( this.config.JonmanResponses.Count) ];
        }

        protected bool MessageContainsMetallica(string message)
        {
            foreach(string word in config.JonmanForbiddenWords)
            {
                Console.WriteLine("forbidden word:" + word);
                if(message.ToLower().Contains(word)){
                    return true;
                }
            }
            return false;
        }

        public string GetName()
        {
            return "jonman";
        }
    }

}
