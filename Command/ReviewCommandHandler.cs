using System;
using DSharpPlus.EventArgs;
using sharpbot.model;
using DSharpPlus.Entities;

namespace sharpbot.Command
{

    public class ReviewCommandHandler : BaseCommand
    {

        private Config config { get; set; }
        public ReviewCommandHandler(Config config)
        {
            this.config = config;
        }

        public bool handle(CommandCollection commands, MessageCreateEventArgs e)
        {
            Console.WriteLine(string.Format("User {0} sent a message in channel {1}", e.Author.Username, e.Channel.Name));
            if( e.Message.ChannelId == this.config.ReviewChannelId)
            {

                // contains attachment or link
                if(e.Message.Attachments.Count > 0 ||  e.Message.Embeds.Count > 0 || this.MessageContainsLink(e.Message.Content)) 
                {
                    if(e.Message.Attachments.Count > 0)
                    {
                        if(this.MessageContainsAudioAttachment(e.Message))
                        {
                            foreach( DiscordAttachment attachment in e.Message.Attachments)
                            {
                                if( !this.CheckMediaFileSize(attachment) )
                                {
                                     e.Message.RespondAsync(string.Format("{0} : Are you sure this mix is longer than a minute ?", e.Author.Mention));
                                     return true;
                                }
                            }
                        }
                        else{
                            if(this.MessageContainsVideoAttachment(e.Message))
                            {
                                e.Message.DeleteAsync().GetAwaiter().GetResult();
                                e.Message.RespondAsync(string.Format("{0} : Please only share audio files", e.Author.Mention));
                                return true;
                            }
                            return false;
                        }
                    }

                    if(this.MessageContainsLink(e.Message.Content) && this.ContainsPublicLink(e.Message.Content))
                    {
                        e.Message.DeleteAsync().GetAwaiter().GetResult();
                        e.Message.RespondAsync(string.Format("{0} : No public links allowed check the rules", e.Author.Mention));
                    }
                    else{
                        
                        e.Message.RespondAsync( string.Format(this.GetResponseMessage(), e.Author.Mention));
                    }

                }
            }
            return false;
        }

        protected bool CheckMediaFileSize( DiscordAttachment attachment)
        {
            int fileSizeInKb = attachment.FileSize / 1024;
            if( attachment.FileName.ToLower().Contains(".mp3") && fileSizeInKb < 2000){
                return false;
            }
            
            if( attachment.FileName.ToLower().Contains(".wav") && fileSizeInKb < 10000){
                return false;
            }

            return true;
        }

        protected bool MessageContainsVideoAttachment(DiscordMessage message)
        {
            foreach( DiscordAttachment attachment in message.Attachments)
            {
                if(attachment.Url.ToLower().Contains(".mp4") || attachment.Url.ToLower().Contains(".webm") || attachment.Url.ToLower().Contains(".avi"))
                {
                    return true;
                }
            }

            return false;

        }

        protected bool MessageContainsAudioAttachment(DiscordMessage message)
        {
            foreach( DiscordAttachment attachment in message.Attachments)
            {
                if(attachment.Url.Contains(".mp3") || attachment.Url.Contains(".wav") || attachment.Url.Contains(".ogg"))
                {
                    return true;
                }
            }

            return false;
        }

        protected string GetResponseMessage()
        {
            Random  rand = new Random();
            return this.config.ReviewReminderMessages[ rand.Next( this.config.ReviewReminderMessages.Count) ];
        }
        protected bool MessageContainsLink(string message)
        {
            if (message.ToLower().Contains("http://"))
            {
                return true;
            }

            if (message.ToLower().Contains("https://"))
            {
                return true;
            }

            return false;
        }

        protected bool ContainsPublicLink(string message)
        {
            if (message.ToLower().Contains("youtube"))
            {
                return true;
            }
            
            if (message.ToLower().Contains("youtu.be"))
            {
                return true;
            }

            if (message.ToLower().Contains("soundcloud"))
            {
                return true;
            }
            return false;

        }

        public string GetName()
        {
            return "mmr";
        }
    }

}
