using System;
using DSharpPlus.EventArgs;
using sharpbot.model;

namespace sharpbot.Command
{
    public interface BaseCommand
    {
        
        bool handle (CommandCollection commands, MessageCreateEventArgs e);

        string GetName();

    }
}